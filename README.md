# d.js v14 base

A simple Discord.js version 14 base. It contains a slash command handler, event handler, and an example for the new modals.

### Config

.env

```
BOT_TOKEN=<your bot token>
BOT_ID=<your bot id>
GUILD_ID=<the server to register commands in>
```

### Utilities

`client.utils.addPermission("commandName", "role id", client)` - Allows a role access to the specific slash command<br>
`client.utils.addPermissionToUser("commandName", "user id", client)` - Allows a user access to the specific slash command<br>
`client.utils.wipePermissions("commandName", client)` - Wipes all permissions for the specific slash command<br>

These can be placed anywhere, but will be most useful in `events > ready.js`
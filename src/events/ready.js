const { Handler } = require("../CommandHandler");

let cmdHandler = new Handler();
// Pass in "true" if you haven't updated any commands (name, description, arguments)
// This prevents rate limits from Discord if you're constantly restarting the bot.
cmdHandler.setup(true);

module.exports = {
  name: "ready",
  async execute() {
    logger.debug({
      title: `Ready`,
      messages: [`Bot is now online!`],
    });
  },
};

module.exports.getCmdHandler = function () {
  return cmdHandler;
};

module.exports = {
  name: "example_form",
  async execute(client, interaction) {
    // Get the fields input value
    let shortExample = interaction.fields.getTextInputValue("example_short");
    interaction.reply(`Short example: ${shortExample}`);
  },
};

const { getCmdHandler } = require("./ready");
const { existsSync } = require("fs");
const { join } = require("path");

let cmdHandler = getCmdHandler();

module.exports = {
  name: "interactionCreate",
  async execute(interaction, client) {
    if (
      !interaction.isContextMenuCommand() &&
      !interaction.isCommand() &&
      !interaction.isSelectMenu() &&
      !interaction.isButton() &&
      !interaction.isAutocomplete() &&
      !interaction.isModalSubmit()
    )
      return;

    if (interaction.isSelectMenu()) {
      if (existsSync(join(__dirname, `menu`, `${interaction.customId}.js`)))
        require(join(__dirname, `menu`, `${interaction.customId}.js`)).execute(
          client,
          interaction
        );
      return;
    }

    if (interaction.isModalSubmit()) {
      if (existsSync(join(__dirname, `modal`, `${interaction.customId}.js`)))
        require(join(__dirname, `modal`, `${interaction.customId}.js`)).execute(
          client,
          interaction
        );
      return;
    }

    if (interaction.isButton()) {
      if (existsSync(join(__dirname, `button`, `${interaction.customId}.js`)))
        await require(join(
          __dirname,
          `button`,
          `${interaction.customId}.js`
        )).execute(client, interaction);
      return;
    }

    if (interaction.isAutocomplete()) {
      if (
        existsSync(
          join(__dirname, `autocomplete`, `${interaction.commandName}.js`)
        )
      )
        await require(join(
          __dirname,
          `autocomplete`,
          `${interaction.commandName}.js`
        )).execute(client, interaction);
      return;
    }

    // Essentially this is the message create event
    cmdHandler
      .getCommand(interaction.commandName)
      .then((cmd) => {
        try {
          const cmdfile = require(cmd["file"]);
          cmdfile.execute(client, interaction);
        } catch (err) {
          console.log(err);
          console.log(`Error executing a command`);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
};

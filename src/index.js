// get everything out of the .env
require("dotenv").config();

const { Client, GatewayIntentBits, Partials } = require("discord.js");
const axios = require("axios").default;
const { readdirSync } = require("fs");
const { join } = require("path");
const astridlogger = require("astridlogger").Logger;

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.GuildMessageReactions,
  ],
  partials: [Partials.Message, Partials.Reaction],
});

client.utils = require("./utils/other-util");
global.logger = new astridlogger({});

// Event handling
const eventFiles = readdirSync(join(__dirname, "events")).filter((file) =>
  file.endsWith(".js")
);

for (const file of eventFiles) {
  const event = require(join(__dirname, "events", file));
  if (event.noLoad) continue;
  if (event.once) {
    client.once(event.name, (...args) => {
      event.execute(...args, client);
    });
  } else {
    client.on(event.name, (...args) => {
      event.execute(...args, client);
    });
  }
}

// If there's no token for some reason, send a message and exit the bot.
if (!process.env.BOT_TOKEN) {
  logger.debug({
    title: `Main process`,
    messages: [`Could not find Discord token!`],
  });
  process.exit(0);
}

// Good practice catching uncaught promise rejections
process.on("unhandledRejection", (error) => {
  logger.debug({
    title: `Main process`,
    messages: [
      `Uncaught Promise Error!\\n ${error["stack"] ? error["stack"] : error}\``,
    ],
  });
});

// Login to the bot
client.login(process.env.BOT_TOKEN);

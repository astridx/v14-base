const { join } = require("path");
const { REST } = require("@discordjs/rest");
const { Routes } = require("discord-api-types/v9");
const async = require("async");

class CommandManager {
    // new CommandManager()

    constructor() {
        this.commands = [];
    }

    async getCommand(name) {
        return new Promise((resolve, reject) => {
            // Loop through each object in the array
            this.commands.forEach((obj) => {
                // If an object exists with the key of the specified name, log stuff.
                if (obj[name]) {
                    resolve(obj[name]);
                }
            });
            reject(`Could not find a command with the name: ${name}`);
        });
    }

    async setup(noload) {
        return new Promise(async (resolve, reject) => {
            let timeStarted = Date.now();
            const commands = this.commands;

            let tempCommands = [];
            // Get all the commands within the slashCommands folder
            const dir = require("node-dir");
            dir.files(join(__dirname, "commands"), function (err, files) {
                if (err) throw err;
                //we have an array of files now, so now we can iterate that array
                files.filter((r) => r.endsWith(".js"));

                function loadCommand(file, callback) {
                    const command = require(file);
                    //  If there's no command name, return
                    if (!command.name) return callback();
                    // 	If it's a subcommand, return
                    if (command.subCommand) return callback();
                    // 	If file is disabled, log it and then continue to the next file
                    if (file.includes("_disabled")) {
                        console.log(`${command.name} is disabled globally`);
                        return callback();
                    }
                    // Create the command
                    let cmd = {
                        name: command.name,
                        description: command.description,
                    };
                    // If command options exist, add it to the cmd object
                    if (command["options"])
                        cmd["options"] = [...command.options];

                    if (command["disabled"]) cmd["default_permission"] = false;

                    if (command["type"]) cmd["type"] = command["type"];

                    // Push cmd to the array
                    tempCommands.push(cmd);

                    let obj = { [cmd.name]: { file } };
                    commands.push(obj);

                    callback();
                }

                async
                    .each(files, loadCommand)
                    .then(async () => {
                        try {
                            if (!noload) {
                                console.log(
                                    "Started refreshing application (/) commands."
                                );
                                const rest = new REST({
                                    version: "9",
                                }).setToken(process.env.BOT_TOKEN);

                                await rest.put(
                                    Routes.applicationGuildCommands(
                                        process.env.BOT_ID,
                                        process.env.GUILD_ID
                                    ),
                                    { body: tempCommands }
                                );

                                console.log(
                                    `Successfully reloaded application (/) commands. (${
                                        Date.now() - timeStarted
                                    }ms)`
                                );
                            }
                        } catch (error) {
                            console.error(error);
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            });
            resolve("Finished");
        });
    }
}

module.exports.Handler = CommandManager;

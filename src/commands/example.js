const { Embed } = require("discord.js");
module.exports = {
  name: "example",
  description: "Basic command example",
  async execute(client, interaction) {
    // Reply
    await interaction.reply({
      content: `Basic example command`,
    });
    // Follow up with an embed
    // Embeds haven't really changed much to my knowledge, besides the class name
    let embed = new Embed().setTitle("Example");
    embed.setDescription("Example embed");
    // You have to await it
    // If you follow up before the reply is finished it would error
    await interaction.followUp({
      embeds: [embed],
    });
  },
};

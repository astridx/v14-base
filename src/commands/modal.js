const { Modal, TextInputComponent, ActionRow } = require("discord.js");
const { TextInputStyle } = require("discord.js");

module.exports = {
  name: "modal",
  description: "Basic modal example",
  async execute(client, interaction) {
    // Create the modal
    const modal = new Modal().setTitle("Example").setCustomId("example_form");

    // Create text input fields

    // Short input
    const shortComponent = new TextInputComponent()
      .setCustomId("example_short")
      .setLabel("Short example")
      .setStyle(TextInputStyle.Short);

    // Long input
    const longComponent = new TextInputComponent()
      .setCustomId("example_paragraph")
      .setLabel("Long example")
      .setStyle(TextInputStyle.Paragraph);

    // Create rows
    const rows = [shortComponent, longComponent].map((component) =>
      new ActionRow().addComponents(component)
    );

    // Add action rows to form
    modal.addComponents(...rows);

    // Present the modal to the user
    await interaction.showModal(modal);
  },
};

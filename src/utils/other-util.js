// Gives a role access to a slash command
async function addPermission(commandName, role, client) {
  if (!client.application?.owner) await client.application?.fetch();
  let commands = await client.guilds.cache
    .get(process.env.GUILD_ID)
    ?.commands.fetch();
  commands.map(async (cmd) => {
    if (cmd.name === commandName) {
      const permissions = [
        {
          id: role,
          type: "ROLE",
          permission: true,
        },
      ];

      try {
        await cmd.permissions.add({ permissions });
        console.log(`Added permission for ${cmd.name}`);
      } catch (err) {
        console.log(err);
      }
    }
  });
}

// Gives a specific user access to a slash command
async function addPermissionToUser(commandName, user, client) {
  if (!client.application?.owner) await client.application?.fetch();
  let commands = await client.guilds.cache
    .get(process.env.GUILD_ID)
    ?.commands.fetch();
  commands.map(async (cmd) => {
    if (cmd.name === commandName) {
      const permissions = [
        {
          id: user,
          type: "USER",
          permission: true,
        },
      ];

      try {
        await cmd.permissions.add({ permissions });
        console.log(`Added permission for ${cmd.name}`);
      } catch (err) {
        console.log(err);
      }
    }
  });
}

// Wipes all permissions for the specified slash command
async function wipePermissions(commandName, client) {
  if (!client.application?.owner) await client.application?.fetch();
  let commands = await client.guilds.cache
    .get(process.env.GUILD_ID)
    ?.commands.fetch();
  commands.map(async (cmd) => {
    if (cmd.name === commandName) {
      try {
        await cmd.permissions.set({ command: cmd.id, permissions: [] });
        console.log(`Wiped permissions for ${cmd.name}`);
      } catch (err) {
        console.log(err);
      }
    }
  });
}

module.exports = {
  wipePermissions,
  addPermission,
  addPermissionToUser,
};
